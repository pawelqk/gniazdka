package tb.sockets.client;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class CClientButton extends JButton implements ActionListener{

	public CClientButton(int row, int column, CClientPanel panel){
		this.row = row;
		this.column = column;
		this.panel = panel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(!clicked_once && enabled){
			setText("X");
			setForeground(Color.blue);
			clicked_once = true;
			CClientButton [][] bts = panel.getButtons();
			
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					bts[i][j].setEnabled(false);
				}
			}
			ClientFrame.sendMove(((CClientButton) e.getSource()).getRow(), ((CClientButton) e.getSource()).getColumn());
			ClientFrame.getMove();
		}	
	}
	
	public void enemyClicked(){
		if(!clicked_once){
			setText("O");
			setForeground(Color.red);
			clicked_once = true;
			CClientButton [][] bts = panel.getButtons();
			
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					bts[i][j].setEnabled(true);
				}
			}
		}
	}
	
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getColumn(){
		return column;
	}
	
	private int row;
	private int column;
	private boolean enabled = true;
	private CClientPanel panel;
	private boolean clicked_once = false;
	private static final long serialVersionUID = 1L;
}
