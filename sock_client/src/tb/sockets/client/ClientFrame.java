package tb.sockets.client;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import tb.sockets.server.CGame;

public class ClientFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnConnect;
	private JLabel lblNotConnected, lblHost, lblPort;
	private JFormattedTextField ftf_ip, ftf_port;
	private static CClientPanel panel;
	
	private static Socket sock = null;
	private static DataOutputStream os = null;
	private static DataInputStream in = null;
	private static int counter = 9;
	private String ip, port;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientFrame frame = new ClientFrame("Lab5");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ClientFrame(String s) {
		super(s);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);
		
		try {
			ftf_ip = new JFormattedTextField(new MaskFormatter("###.###.###.###"));
			ftf_ip.setBounds(43, 11, 90, 20);
			ftf_ip.setText("xxx.xxx.xxx.xxx");
			contentPane.add(ftf_ip);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 90, 23);
		contentPane.add(btnConnect);
		btnConnect.addActionListener(this);
		
		ftf_port = new JFormattedTextField();
		ftf_port.setText("xxxx");
		ftf_port.setBounds(43, 39, 90, 20);
		contentPane.add(ftf_port);
		lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		
		panel = new CClientPanel();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		lblNotConnected = new JLabel(" Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnConnect)
			try {
				ip = ftf_ip.getText();
				port = ftf_port.getText();
				sock = new Socket(ip, Integer.parseInt(port));
				os = new DataOutputStream(sock.getOutputStream());
				in = new DataInputStream(sock.getInputStream());
				lblNotConnected.setText("Connected");
				lblNotConnected.setBackground(Color.green);
				getMove();
			} catch (IOException e3) {
				lblNotConnected.setText("Not Connected");
				lblNotConnected.setBackground(Color.red);
			}
	}
	
	public static void sendMove(int r, int c) {
		checkWinner();
		if (counter > 0) {
			counter--;
			try {
				os.writeInt(r);
				os.writeInt(c);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		checkWinner();
	}
	
	public static void getMove() {
		if(counter > 0){
			int i = 0; int j = 0;
			try {
				i = in.readInt();
				j = in.readInt();
			} catch (IOException e) {
				e.printStackTrace();
			}
			checkWinner();
			panel.getButtons()[i][j].enemyClicked();
			counter--;
		}
		else{
			JOptionPane.showMessageDialog(null, "Draw!");
			System.exit(0);
		}
	}
	
	public static void checkWinner() {
		if(panel.checkWin() == "X" || panel.checkWin() == "O"){
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					panel.getButtons()[i][j].setEnabled(false);
				}
			}
			if(panel.checkWin() == "X"){
				JOptionPane.showMessageDialog(null, "You win!");
				System.exit(0);
			}
			else{
				JOptionPane.showMessageDialog(null, "You lose!");
				System.exit(0);
			}
		}
	}
}
