package tb.sockets.client;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;


public class CClientPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public CClientPanel(){
		setOpaque(true);
		setBackground(Color.WHITE);
		setLayout(null);
		addComponents();
	}
	
	public CClientButton [][] getButtons(){
		return buttons;
	}
	
	private void addComponents(){
		final int px = 100;
		Font font = new Font("Arial", Font.BOLD, 80);
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				buttons[i][j] = new CClientButton(i, j, this);
				buttons[i][j].setFont(font);
				buttons[i][j].setBackground(Color.WHITE);
				buttons[i][j].addActionListener(buttons[i][j]);
				buttons[i][j].setBounds(i*(5+px), j*(5+px), px, px);
				add(buttons[i][j]);
			}
		}
	}
	
	public String checkWin(){
		int count = 0;
		String winner = null;
		boolean won = false;
		
		for(int i = 0; i < 3 && !won; i++){
			for(int j = 0; j < 3; j++){
				if(buttons[i][j].getText().equals("X")) count++;
				else if(buttons[i][j].getText().equals("O")) count--;
			}
			if(count == 3){
				won = true;
				winner = "X";
			}
			else if(count == -3){
				won = true;
				winner = "O";
			}
			count = 0;
		}
		
		for(int i = 0; i < 3 && !won; i++){
			for(int j = 0; j < 3; j++){
				if(buttons[j][i].getText().equals("X")) count++;
				else if(buttons[j][i].getText().equals("O")) count--;
			}
			if(count == 3){
				won = true;
				winner = "X";
			}
			else if(count == -3){
				won = true;
				winner = "O";
			}
			count = 0;
		}
			
		for(int i = 0; i < 3 && !won; i++) {
			if(buttons[i][i].getText().equals("X")) count++;
			else if(buttons[i][i].getText().equals("O")) count--;
		}
		
		if(count == 3){
			won = true;
			winner = "X";
		}
		else if(count == -3){
			won = true;
			winner = "O";
		}
		count = 0;
		
		for(int i = 0; i < 3 && !won; i++) {
			if(buttons[2-i][i].getText().equals("X")) count++;
			else if(buttons[2-i][i].getText().equals("O")) count--;
		}
		
		if(count == 3){
			won = true;
			winner = "X";
		}
		else if(count == -3){
			won = true;
			winner = "O";
		}
		
		return winner;
	}
	
	CClientButton [][] buttons = new CClientButton[3][3];
}
