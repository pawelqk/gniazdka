package tb.sockets.server;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;


public class CServerButton extends JButton implements ActionListener{

	public CServerButton(int row, int column, CServerPanel panel){
		this.row = row;
		this.column = column;
		this.panel = panel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(!clicked_once && enabled){
			setText("O");
			setForeground(Color.blue);
			clicked_once = true;
			CGame.checkWin(panel);
			CServerButton [][] bts = panel.getButtons();
			
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					bts[i][j].setEnabled(false);
				}
			}
			
			ServerFrame.sendMove(((CServerButton) e.getSource()).getRow(), ((CServerButton) e.getSource()).getColumn());
			ServerFrame.getMove();
		}	
	}
	
	public void enemyClicked(){
		if(!clicked_once){
			setText("X");
			setForeground(Color.red);
			clicked_once = true;
			CGame.checkWin(panel);
			CServerButton [][] bts = panel.getButtons();
			
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					bts[i][j].setEnabled(true);
				}
			}
		}
	}
	
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}
	public int getRow(){
		return row;
	}
	
	public int getColumn(){
		return column;
	}
	
	private int row;
	private int column;
	private boolean enabled = true;
	private CServerPanel panel;
	private boolean clicked_once = false;
	private static final long serialVersionUID = 1L;
}
