package tb.sockets.server;

public class CGame {
	
	public static String checkWin(CServerPanel panel){
		int count = 0;
		CServerButton [][] bts = panel.getButtons();
		boolean won = false;
		
		for(int i = 0; i < 3 && !won; i++){
			for(int j = 0; j < 3; j++){
				if(bts[i][j].getText().equals("X")) count++;
				else if(bts[i][j].getText().equals("O")) count--;
			}
			if(count == 3){
				won = true;
				winner = "X";
			}
			else if(count == -3){
				won = true;
				winner = "O";
			}
			count = 0;
		}
		
		for(int i = 0; i < 3 && !won; i++){
			for(int j = 0; j < 3; j++){
				if(bts[j][i].getText().equals("X")) count++;
				else if(bts[j][i].getText().equals("O")) count--;
			}
			if(count == 3){
				won = true;
				winner = "X";
			}
			else if(count == -3){
				won = true;
				winner = "O";
			}
			count = 0;
		}
			
		for(int i = 0; i < 3 && !won; i++) {
			if(bts[i][i].getText().equals("X")) count++;
			else if(bts[i][i].getText().equals("O")) count--;
		}
		
		if(count == 3){
			won = true;
			winner = "X";
		}
		else if(count == -3){
			won = true;
			winner = "O";
		}
		count = 0;
		
		for(int i = 0; i < 3 && !won; i++) {
			if(bts[2-i][i].getText().equals("X")) count++;
			else if(bts[2-i][i].getText().equals("O")) count--;
		}
		
		if(count == 3){
			won = true;
			winner = "X";
		}
		else if(count == -3){
			won = true;
			winner = "O";
		}
		
		return winner;
	}
	private static String winner = null;	
}
