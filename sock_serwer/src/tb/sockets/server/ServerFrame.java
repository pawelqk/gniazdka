package tb.sockets.server;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class ServerFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static ServerSocket sSock = null;
	private static Socket sock = null;
	private static DataInputStream in = null;
	private static DataOutputStream os = null;
	
	private static int counter = 9;
	private JPanel contentPane;
	private JLabel lblNotConnected;
	private JButton btnCreate;
	private JFormattedTextField ftf_port;
	private static CServerPanel panel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run()	 {
				try {
					ServerFrame frame = new ServerFrame("Lab5");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public ServerFrame(String s){
		super(s);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		btnCreate = new JButton("Run");
		btnCreate.setBounds(10, 70, 90, 23);
		contentPane.add(btnCreate);
		btnCreate.addActionListener(this);
		ftf_port = new JFormattedTextField();
		ftf_port.setText("Insert Port");
		ftf_port.setBounds(10, 39, 90, 20);
		contentPane.add(ftf_port);
		panel = new CServerPanel();
		panel.setBounds(150, 0, 550, 460);
		contentPane.add(panel);
		lblNotConnected = new JLabel(" Enemy Not Connected ");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 227, 130, 23);
		contentPane.add(lblNotConnected);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCreate)
			try {
				sSock = new ServerSocket(Integer.parseInt(ftf_port.getText()));
				sock = sSock.accept();
				enemyConnected();
				in = new DataInputStream(sock.getInputStream());
				os = new DataOutputStream(sock.getOutputStream());
			} catch (IOException e3) {
				e3.printStackTrace();
			}
	}
	
	public static void sendMove(int r, int c) {
		checkWinner();
		if (counter > 0) {
			counter--;
			try {
				os.writeInt(r);
				os.writeInt(c);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void getMove() {
		if(counter > 0){
			int i = 0; int j = 0;
			try {
				i = in.readInt();
				j = in.readInt();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			panel.getButtons()[i][j].enemyClicked();
			counter--;
			checkWinner();
		}
		else{
			JOptionPane.showMessageDialog(null, "Draw!");
			System.exit(0);
		}
	}
	
	public static void checkWinner() {
		if(panel.checkWin() == "X" || panel.checkWin() == "O"){
			for(int i = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					panel.getButtons()[i][j].setEnabled(false);
				}
			}
			if(panel.checkWin() == "O"){
				JOptionPane.showMessageDialog(null, "You win!");
				System.exit(0);
			}
			else{
				JOptionPane.showMessageDialog(null, "You lose!");
				System.exit(0);
			}
		}
	}
	
	private void enemyConnected() {
		lblNotConnected.setText(" Enemy Connected ");
		lblNotConnected.setForeground(Color.black);
		lblNotConnected.setBackground(Color.green);
	}
}
